package io.axual.training;

import io.axual.training.properties.StreamsProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.Arrays;

@SpringBootApplication
public class StreamCommandLineRunner implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(StreamCommandLineRunner.class);

    private final StreamsProperties streamsProperties;

    @Autowired
    public StreamCommandLineRunner(StreamsProperties streamsProperties) {
        this.streamsProperties = streamsProperties;
    }

    public static void main(String... args) throws Exception {
        new SpringApplicationBuilder(StreamCommandLineRunner.class).web(false).run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        AlertStreamRunner alertStreamRunner = new AlertStreamRunner(streamsProperties);
        alertStreamRunner.start();
        logger.info("Application started with command-line arguments: {} . \n To kill this application, press Ctrl + C.", Arrays.toString(args));
    }
}
