# Financial alerting application

This is the financial alerting streaming application, that combines information from 2 streams, namely "payments-accountentry" and "payments-accountauthorization", determines whether account balances drop below or jump above a certain threshold, and generates alert events for it on the "interaction-alert" stream. This application uses Kafka Streams library version 1.0.0.

An example AlertEvent is shown below:

Balance below zero example:

```
{
	"timestamp": 1516821348826,
	"source": {
		"name": "io.axual.training.AlertStreamRunner",
		"version": {
			"string": "1.0.0-SNAPSHOT"
		}
	},
	"alertType": "balance-below-zero",
	"user": {
		"userId": "axual-demo-id-14",
		"customerId": "axual-demo-id-14"
	},
	"channel": "PUSH",
	"params": {
		"amount": "€ 341,75",
		"balanceAfter": "€ -255,94",
		"iban": "NL14RABO0000000014"
	}
} 
```
Balance above zero example:
```
{
	"timestamp": 1516821352842,
	"source": {
		"name": "io.axual.training.AlertStreamRunner",
		"version": {
			"string": "1.0.0-SNAPSHOT"
		}
	},
	"alertType": "balance-above-zero",
	"user": {
		"userId": "axual-demo-id-14",
		"customerId": "axual-demo-id-14"
	},
	"channel": "PUSH",
	"params": {
		"amount": "€ 721,55",
		"balanceAfter": "€ 465,61",
		"iban": "NL14RABO0000000014"
	}
}
```

## Usage:
1. Use the "prepare-alert.sh" script in the root of the confluent-platform repo to make sure the topic "interaction-alert" exists
2. Modify application-default.yml where needed, e.g. when your broker or schema registry node are on a different address.
3. Start the application to generate financial alerts based on transaction on bank accounts
```
mvn clean package spring-boot:run
```
4. Use the "check-alert.sh" script to check the interaction-alert topic whether it was successful. Output like shown above should be visible.